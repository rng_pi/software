#!/usr/bin/python3
import time

from egg_device import *

egg = EggDevice()
for i in range(5):
    egg.set_led_state(i, OFF)

while True:
    for i in range(4):
        egg.set_led_state(i, ON)
        time.sleep(0.1)

    for i in range(4):
        egg.set_led_state(i, OFF)
        time.sleep(0.5)
