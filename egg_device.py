#!/usr/bin/python3
import binascii
import shlex
import subprocess
from dataclasses import dataclass
from datetime import datetime, timezone

import spidev

import data_packet_pb2

ON = 1
OFF = 0


def save_to_log(msg):
    utc_date = datetime.now(timezone.utc).strftime("(%Y.%m.%d - %H:%M:%S): ")
    with open("/dev/shm/rng_log", 'a') as f:
        f.write(utc_date + msg + "\n")


def get_serial_number():
    sn = "0000000000000000"
    f = open('/proc/cpuinfo', 'r')
    for line in f:
        if line[0:6] == 'Serial':
            sn = line[10:26]
    f.close()

    return sn


def get_rpi_utc_time():
    utc_date = datetime.now(timezone.utc).strftime("%Y.%m.%d.%H.%M.%S")
    year = int(utc_date.split('.')[0])
    month = int(utc_date.split('.')[1])
    day = int(utc_date.split('.')[2])
    hour = int(utc_date.split('.')[3])
    minute = int(utc_date.split('.')[4])
    sec = int(utc_date.split('.')[5])
    return year, month, day, hour, minute, sec


def get_crc32_of_file(filename):
    buf = open(filename, 'rb').read()
    buf = (binascii.crc32(buf) & 0xFFFFFFFF)
    return buf


def run_cmd(cmd, w_dir, wait=True):
    p = subprocess.Popen(
        shlex.split(cmd),
        cwd=w_dir
    )
    if wait:
        p.wait()
    else:
        return p


def run_cmd_and_get_output(cmd, w_dir):
    p = subprocess.Popen(
        shlex.split(cmd),
        cwd=w_dir,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    output = p.stdout.read()
    return str(output)


@dataclass
class ChannelDataPacket:
    ch_enable: bool = False
    last_ts_nanosec: int = None
    base_ts_sec: int = None
    base_ts_nanosec: int = None
    data_packet = [data_packet_pb2.DataPacket(), data_packet_pb2.DataPacket()]
    active_dp = 0


class SPI:
    def __init__(self, bus, device, max_speed_hz, CPOL=0, CPHA=0):
        self.spi = spidev.SpiDev()
        self.spi.open(bus, device)
        self.spi.max_speed_hz = max_speed_hz
        self.spi.mode = CPOL * 2 + CPHA

    def write_data(self, addr, val):
        addr3 = int(((addr >> 24) & 0xFF) | 0x80)
        addr2 = int((addr >> 16) & 0xFF)
        addr1 = int((addr >> 8) & 0xFF)
        addr0 = int((addr >> 0) & 0xFF)
        val3 = int((val >> 24) & 0xFF)
        val2 = int((val >> 16) & 0xFF)
        val1 = int((val >> 8) & 0xFF)
        val0 = int((val >> 0) & 0xFF)

        msg = [addr3, addr2, addr1, addr0, val3, val2, val1, val0]
        #print(msg)
        result = self.spi.xfer2(msg)

    def read_data(self, addr, byte_len=4):
        addr3 = int(((addr >> 24) & 0xFF) & 0x7F)
        addr2 = int((addr >> 16) & 0xFF)
        addr1 = int((addr >> 8) & 0xFF)
        addr0 = int((addr >> 0) & 0xFF)

        msg = [addr3, addr2, addr1, addr0, 0x00, 0x00, 0x00, 0x00]
        for i in range(byte_len):
            msg.append(0x0)
        result = self.spi.xfer2(msg)
        #print(result)
        #res = ''.join(format(x, '02x') for x in result)
        #print(res)

        if byte_len == 4:
            return result[7], (result[8] << 24) + (result[9] << 16) + (result[10] << 8) + result[11]
        return result[7], result[8:]


class EggDevice:
    S_CTRL_REG = 0x00
    # bit numbers
    LED = [0, 1, 4, 3, 2]
    UART_EN = 5

    A_CTRL_REG = 0x01
    # bit numbers
    RST = 0
    TEST = 31  # fake pps enable

    CH = [0x10, 0x20, 0x30, 0x40]

    CTRL0_REG = 0x00  # enable channel on bit 0
    CTRL1_REG = 0x01  # number of bits to count
    CONF_XOR_REG = [None, None, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]

    CH_DATA = [0x1000, 0x1100, 0x1200, 0x1300]

    SUBCH_BYTE_LEN = 6

    GPS_DATA = 0x100

    def __init__(self):
        self.spi = SPI(bus=0, device=0, max_speed_hz=5*1000*1000)

    def _set_bit_of_reg(self, addr, bit_num):
        status, reg_val = self.spi.read_data(addr)
        new_reg_val = reg_val | (1 << bit_num)
        self.spi.write_data(addr, new_reg_val)

    def _clear_bit_of_reg(self, addr, bit_num):
        status, reg_val = self.spi.read_data(addr)
        new_reg_val = reg_val & (~(1 << bit_num))
        self.spi.write_data(addr, new_reg_val)

    def set_led_state(self, led_num, state):
        if state == 1:
            self._set_bit_of_reg(self.S_CTRL_REG, self.LED[led_num])
        elif state == 0:
            self._clear_bit_of_reg(self.S_CTRL_REG, self.LED[led_num])

    def uart_enable_ctrl(self, state):
        if state == 1:
            self._set_bit_of_reg(self.S_CTRL_REG, self.UART_EN)
        elif state == 0:
            self._clear_bit_of_reg(self.S_CTRL_REG, self.UART_EN)

    def reset(self):
        self._set_bit_of_reg(self.A_CTRL_REG, self.RST)
        self._clear_bit_of_reg(self.A_CTRL_REG, self.RST)

    def test_enable_ctrl(self, state):
        if state == 1:
            self._set_bit_of_reg(self.A_CTRL_REG, self.TEST)
        elif state == 0:
            self._clear_bit_of_reg(self.A_CTRL_REG, self.TEST)

    def ch_enable_ctrl(self, channel, state):
        if state == 1:
            self._set_bit_of_reg(self.CH[channel] + self.CTRL0_REG, 0)
        elif state == 0:
            self._clear_bit_of_reg(self.CH[channel] + self.CTRL0_REG, 0)

    def ch_pseudo_rng_enable_ctrl(self, channel, state):
        if state == 1:
            self._set_bit_of_reg(self.CH[channel] + self.CTRL0_REG, 1)
        elif state == 0:
            self._clear_bit_of_reg(self.CH[channel] + self.CTRL0_REG, 1)

    def set_num_of_bits_to_count(self, channel, num):
        self.spi.write_data(self.CH[channel] + self.CTRL1_REG, num)

    def set_xor_val(self, channel, subchannel, val):
        if 2 <= subchannel <= 7:
            self.spi.write_data(self.CH[channel] + self.CONF_XOR_REG[subchannel], val)

    def read_status(self):
        s, d = self.spi.read_data(self.CH_DATA[0])
        return s

    def read_channel_data(self, channel):
        return self.spi.read_data(self.CH_DATA[channel], 60)

    def read_gps_data(self):
        s, d = self.spi.read_data(self.GPS_DATA, 24)

        lat_deg = ''
        lat_deg += str(chr(d[0]))
        lat_deg += str(chr(d[1]))

        lat_min = ''
        for i in range(2, 10):
            lat_min += str(chr(d[i]))

        ns_ind = str(chr(d[10]))

        lon_deg = ''
        lon_deg += str(chr(d[11]))
        lon_deg += str(chr(d[12]))
        lon_deg += str(chr(d[13]))

        lon_min = ''
        for i in range(14, 22):
            lon_min += str(chr(d[i]))

        ew_ind = str(chr(d[22]))

        quality = str(chr(d[23]))

        return lat_deg, lat_min, ns_ind, lon_deg, lon_min, ew_ind, quality

    @staticmethod
    def parse_timestamp(d):
        date = ''
        date += str(chr(d[0]))
        date += str(chr(d[1]))
        date += ":"
        date += str(chr(d[2]))
        date += str(chr(d[3]))
        date += ":"
        date += str(chr(d[4]))
        date += str(chr(d[5]))

        ns = (d[6] << 24) + (d[7] << 16) + (d[8] << 8) + d[9]

        return date, ns

    @staticmethod
    def parse_timestamp_sec_nanosec(d):
        h = ''
        h += str(chr(d[0]))
        h += str(chr(d[1]))
        m = ''
        m += str(chr(d[2]))
        m += str(chr(d[3]))
        s = ''
        s += str(chr(d[4]))
        s += str(chr(d[5]))

        try:
            hour_gps = int(h)
            minute_gps = int(m)
            sec_gps = int(s)
            nanosec_gps = (d[6] << 24) + (d[7] << 16) + (d[8] << 8) + d[9]
        except:
            return 0, 0

        year_rpi, month_rpi, day_rpi, hour_rpi, minute_rpi, sec_rpi = get_rpi_utc_time()

        dt = datetime(year_rpi, month_rpi, day_rpi, hour_gps, minute_gps, sec_gps)
        if abs(hour_rpi - hour_gps) <= 1:
            timestamp_sec = int(dt.replace(tzinfo=timezone.utc).timestamp())
        elif hour_rpi > hour_gps:
            timestamp_sec = int(dt.replace(tzinfo=timezone.utc).timestamp()) + 24*3600
        elif hour_rpi < hour_gps:
            timestamp_sec = int(dt.replace(tzinfo=timezone.utc).timestamp()) - 24*3600

        return timestamp_sec, nanosec_gps

    @staticmethod
    def parse_subch_data(d):
        return (d[0] << 40) + (d[1] << 32) + (d[2] << 24) + (d[3] << 16) + (d[4] << 8) + d[5]

    @staticmethod
    def parse_channel_data(d):
        date, ns = EggDevice.parse_timestamp(d[0:10])
        sum_a0 = EggDevice.parse_subch_data(d[10:16])
        sum_a1 = EggDevice.parse_subch_data(d[16:22])
        sum_a2 = EggDevice.parse_subch_data(d[22:28])
        sum_a3 = EggDevice.parse_subch_data(d[28:34])
        sum_a4 = EggDevice.parse_subch_data(d[34:40])
        sum_a5 = EggDevice.parse_subch_data(d[40:46])
        sum_a6 = EggDevice.parse_subch_data(d[46:52])
        sum_a7 = EggDevice.parse_subch_data(d[52:58])

        print("date:", date)
        print("ns:", float(ns / float(1000000000)))
        print("sum_a0:", sum_a0)
        print("sum_a1:", sum_a1)
        print("sum_a2:", sum_a2)
        print("sum_a3:", sum_a3)
        print("sum_a4:", sum_a4)
        print("sum_a5:", sum_a5)
        print("sum_a6:", sum_a6)
        print("sum_a7:", sum_a7)

    @staticmethod
    def dump_channel_data_to_json(ch, cfg_id, d):
        date, ns = EggDevice.parse_timestamp(d[0:10])
        sum_a0 = EggDevice.parse_subch_data(d[10:16])
        sum_a1 = EggDevice.parse_subch_data(d[16:22])
        sum_a2 = EggDevice.parse_subch_data(d[22:28])
        sum_a3 = EggDevice.parse_subch_data(d[28:34])
        sum_a4 = EggDevice.parse_subch_data(d[34:40])
        sum_a5 = EggDevice.parse_subch_data(d[40:46])
        sum_a6 = EggDevice.parse_subch_data(d[46:52])
        sum_a7 = EggDevice.parse_subch_data(d[52:58])

        #print("date:", date)
        #print("ns:", float(ns / float(1000000000)))

        json = {"channel": ch,
                "config_id": cfg_id,
                "xor0_sum": sum_a0,
                "xor1_sum": sum_a1,
                "xor2_sum": sum_a2,
                "xor3_sum": sum_a3,
                "xor4_sum": sum_a4,
                "xor5_sum": sum_a5,
                "xor6_sum": sum_a6,
                "xor7_sum": sum_a7}

        return json

    @staticmethod
    def dump_channel_data_to_proto(d, ts_sec, ts_nanosec):
        ch_data = data_packet_pb2.ChannelData()

        ch_data.xor0_sum = EggDevice.parse_subch_data(d[10:16])
        ch_data.xor1_sum = EggDevice.parse_subch_data(d[16:22])
        ch_data.xor2_sum = EggDevice.parse_subch_data(d[22:28])
        ch_data.xor3_sum = EggDevice.parse_subch_data(d[28:34])
        ch_data.xor4_sum = EggDevice.parse_subch_data(d[34:40])
        ch_data.xor5_sum = EggDevice.parse_subch_data(d[40:46])
        ch_data.xor6_sum = EggDevice.parse_subch_data(d[46:52])
        ch_data.xor7_sum = EggDevice.parse_subch_data(d[52:58])
        ch_data.timestamp_sec_diff = ts_sec
        ch_data.timestamp_nanosec_diff = ts_nanosec

        return ch_data
