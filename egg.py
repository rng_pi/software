#!/usr/bin/python3
import multiprocessing
import time
from datetime import datetime, timezone
from egg_device import *
import data_packet_pb2
from dataclasses import dataclass
import sys
import os
import json
from database_adapter import DatabaseAdapter
import inspect


json_file = open(sys.argv[1])
config = json.load(json_file)
cfg_id = [None, None, None, None]
api_url = config['api_url']
db = DatabaseAdapter(api_url)
device_serial_number = get_serial_number()


@dataclass
class SerializedDataInfo:
    ch_num: int = None
    cfg_id: int = None
    file_path: str = None


@dataclass
class GpsPosition:
    valid: bool = None
    lat_deg: str = None
    lat_min: str = None
    ns_ind: str = None
    lon_deg: str = None
    lon_min: str = None
    ew_ind: str = None
    quality: str = None


def get_channel_config_id(cfg_json):
    while True:
        try:
            conf = db.upload_channel_config(num_of_bits_to_count=cfg_json['num_of_bits_to_count'],
                                            xor0_conf=cfg_json['xor0_conf'],
                                            xor1_conf=cfg_json['xor1_conf'],
                                            xor2_conf=cfg_json['xor2_conf'],
                                            xor3_conf=cfg_json['xor3_conf'],
                                            xor4_conf=cfg_json['xor4_conf'],
                                            xor5_conf=cfg_json['xor5_conf'],
                                            xor6_conf=cfg_json['xor6_conf'],
                                            xor7_conf=cfg_json['xor7_conf'])
            return conf['config_id']
        except:
            e = sys.exc_info()[0]
            print("Getting channel configuration error: %s" % e)
            save_to_log("Getting channel configuration error: %s" % e)
            time.sleep(5)


def spi_data_reader(conn_ch0, conn_ch1, conn_ch2, conn_ch3, conn_status):
    conn = [conn_ch0, conn_ch1, conn_ch2, conn_ch3]
    last_ts_nanosec = [None, None, None, None]
    ch_enable = [False, False, False, False]

####INIT#####################################################################
    # while True:
    #     try:
    #         config = db.get_device_config(device_serial_number)
    #     except:
    #         e = sys.exc_info()[0]
    #         print("Error: %s" % e)
    #         time.sleep(10)
    #         continue
    #     break

    egg = EggDevice()
    egg.reset()
    egg.test_enable_ctrl(0)
    for ch in range(4):
        egg.ch_enable_ctrl(channel=ch, state=OFF)
        egg.ch_pseudo_rng_enable_ctrl(channel=ch, state=OFF)

    if config['ch0']:
        egg.set_num_of_bits_to_count(channel=0, num=config['ch0']['num_of_bits_to_count'])
        egg.set_xor_val(channel=0, subchannel=2, val=config['ch0']['xor2_conf'])
        egg.set_xor_val(channel=0, subchannel=3, val=config['ch0']['xor3_conf'])
        egg.set_xor_val(channel=0, subchannel=4, val=config['ch0']['xor4_conf'])
        egg.set_xor_val(channel=0, subchannel=5, val=config['ch0']['xor5_conf'])
        egg.set_xor_val(channel=0, subchannel=6, val=config['ch0']['xor6_conf'])
        egg.set_xor_val(channel=0, subchannel=7, val=config['ch0']['xor7_conf'])
        # egg.ch_pseudo_rng_enable_ctrl(channel=0, state=ON)
        egg.ch_enable_ctrl(channel=0, state=ON)
        ch_enable[0] = True
    if config['ch1']:
        egg.set_num_of_bits_to_count(channel=1, num=config['ch1']['num_of_bits_to_count'])
        egg.set_xor_val(channel=1, subchannel=2, val=config['ch1']['xor2_conf'])
        egg.set_xor_val(channel=1, subchannel=3, val=config['ch1']['xor3_conf'])
        egg.set_xor_val(channel=1, subchannel=4, val=config['ch1']['xor4_conf'])
        egg.set_xor_val(channel=1, subchannel=5, val=config['ch1']['xor5_conf'])
        egg.set_xor_val(channel=1, subchannel=6, val=config['ch1']['xor6_conf'])
        egg.set_xor_val(channel=1, subchannel=7, val=config['ch1']['xor7_conf'])
        # egg.ch_pseudo_rng_enable_ctrl(channel=1, state=ON)
        egg.ch_enable_ctrl(channel=1, state=ON)
        ch_enable[1] = True
    if config['ch2']:
        egg.set_num_of_bits_to_count(channel=2, num=config['ch2']['num_of_bits_to_count'])
        egg.set_xor_val(channel=2, subchannel=2, val=config['ch2']['xor2_conf'])
        egg.set_xor_val(channel=2, subchannel=3, val=config['ch2']['xor3_conf'])
        egg.set_xor_val(channel=2, subchannel=4, val=config['ch2']['xor4_conf'])
        egg.set_xor_val(channel=2, subchannel=5, val=config['ch2']['xor5_conf'])
        egg.set_xor_val(channel=2, subchannel=6, val=config['ch2']['xor6_conf'])
        egg.set_xor_val(channel=2, subchannel=7, val=config['ch2']['xor7_conf'])
        egg.ch_enable_ctrl(channel=2, state=ON)
        ch_enable[2] = True
    if config['ch3']:
        egg.set_num_of_bits_to_count(channel=3, num=config['ch3']['num_of_bits_to_count'])
        egg.set_xor_val(channel=3, subchannel=2, val=config['ch3']['xor2_conf'])
        egg.set_xor_val(channel=3, subchannel=3, val=config['ch3']['xor3_conf'])
        egg.set_xor_val(channel=3, subchannel=4, val=config['ch3']['xor4_conf'])
        egg.set_xor_val(channel=3, subchannel=5, val=config['ch3']['xor5_conf'])
        egg.set_xor_val(channel=3, subchannel=6, val=config['ch3']['xor6_conf'])
        egg.set_xor_val(channel=3, subchannel=7, val=config['ch3']['xor7_conf'])
        egg.ch_enable_ctrl(channel=3, state=ON)
        ch_enable[3] = True

#########################################################################
    prev_utc_valid = None
    while True:
        for ch in range(4):
            if ch_enable[ch]:
                status = egg.read_status()
                utc_valid = True if status & 0x01 else False
                if utc_valid != prev_utc_valid:
                    # print(egg.read_gps_data())
                    lat_deg, lat_min, ns_ind, lon_deg, lon_min, ew_ind, quality = egg.read_gps_data()
                    # if not 0x2F < ord(lat_deg[0]) < 0x3A: TODO: fix
                    #     continue
                    gps_pos = GpsPosition(
                        valid=utc_valid,
                        lat_deg=lat_deg,
                        lat_min=lat_min,
                        ns_ind=ns_ind,
                        lon_deg=lon_deg,
                        lon_min=lon_min,
                        ew_ind=ew_ind,
                        quality=quality
                    )
                    conn_status.send(gps_pos)
                prev_utc_valid = utc_valid
                if (status >> (ch + 1)) & 0x01:  # channel data ready
                    status, data = egg.read_channel_data(channel=ch)
                    if utc_valid:
                        conn[ch].send(data)


def channel_data_packer(conn, ch, conn_to_sender):
    data_packet = [data_packet_pb2.DataPacket(), data_packet_pb2.DataPacket()]  # TODO: check if necessary
    active_dp = 0
    serialize = False
    base_ts_sec = None
    base_ts_nanosec = None

    while True:
        msg = conn.recv()
        ts_sec, ts_nanosec = EggDevice.parse_timestamp_sec_nanosec(msg[0:10])

        if base_ts_sec is None:
            base_ts_sec = ts_sec
            base_ts_nanosec = ts_nanosec

        temp = EggDevice.dump_channel_data_to_proto(msg,
                                                    ts_sec,  # - base_ts_sec,
                                                    ts_nanosec)  # - base_ts_nanosec)
        data_packet[active_dp].data.append(temp)

        sec_num = int(datetime.utcnow().strftime("%S"))
        # if serialize and ts_sec % 60 == (0 + ch*15):
        if serialize and sec_num == 30:  #(0 + ch*15):  # TODO: change 0 to different value (random?)
            serialize = False
            # print("serialize channel", ch)

            data_packet[active_dp].timestamp_sec = base_ts_sec
            data_packet[active_dp].timestamp_nanosec = base_ts_nanosec

            serialized_message = data_packet[active_dp].SerializeToString()
            path = "/dev/shm/" + str(int(datetime.utcnow().replace(tzinfo=timezone.utc).timestamp())) \
                   + "_ch" + str(ch)
            with open(path, "wb") as f:
                f.write(serialized_message)
                f.close()

            info = SerializedDataInfo(  # TODO: change cfg_id
                ch_num=ch,
                cfg_id=cfg_id[ch],
                file_path=path
            )
            conn_to_sender.send(info)

            base_ts_sec = None
            del data_packet[active_dp].data[:]

            active_dp = (active_dp + 1) % 2

        elif sec_num != 30: # (0 + ch*15):  # TODO: change 0 to different value (random?)
            serialize = True


def data_sender(conn):
    while True:
        msg = conn.recv()
        print("Data sender received the message: {} {} {}".format(msg.ch_num,
                                                                  msg.cfg_id,
                                                                  msg.file_path))
        try:
            crc32 = get_crc32_of_file(msg.file_path)
            db.upload_data_binary(serial_number=device_serial_number,
                                  channel=msg.ch_num,
                                  config_id=msg.cfg_id,
                                  data_file_path=msg.file_path,
                                  crc32=crc32)
            os.remove(msg.file_path)
            continue
        except:
            e = sys.exc_info()[0]
            print("Data sending error: %s" % e)
            save_to_log("Data sending error: %s" % e)

        time.sleep(3)
        # try again before give up
        try:
            crc32 = get_crc32_of_file(msg.file_path)
            db.upload_data_binary(serial_number=device_serial_number,
                                  channel=msg.ch_num,
                                  config_id=msg.cfg_id,
                                  data_file_path=msg.file_path,
                                  crc32=crc32)
        except:
            e = sys.exc_info()[0]
            print("Data sending error: %s" % e)
            save_to_log("Data sending error: %s" % e)

        os.remove(msg.file_path)


def status_updater(conn):
    while True:
        msg = conn.recv()
        gps_position = None
        if msg.valid:
            gps_position = "{}°{}'{} {}°{}'{}"\
                .format(msg.lat_deg,
                        msg.lat_min,
                        msg.ns_ind,
                        msg.lon_deg,
                        msg.lon_min,
                        msg.ew_ind)
        try:
            db.update_device_status(serial_number=device_serial_number,
                                    gps_signal_valid=msg.valid,
                                    gps_position=gps_position)
        except:
            e = sys.exc_info()[0]
            print("Status update error: %s" % e)
            save_to_log("Status update error: %s" % e)


# MAIN ######################################################################
# creating pipes
parent_conn_ch0, child_conn_ch0 = multiprocessing.Pipe()
parent_conn_ch1, child_conn_ch1 = multiprocessing.Pipe()
parent_conn_ch2, child_conn_ch2 = multiprocessing.Pipe()
parent_conn_ch3, child_conn_ch3 = multiprocessing.Pipe()
parent_conn_sender, child_conn_sender = multiprocessing.Pipe()
parent_conn_status, child_conn_status = multiprocessing.Pipe()

# exit if every channel is disabled
if not config['ch0'] and not config['ch1'] and not config['ch2'] and not config['ch3']:
    exit()

# creating new processes
if config['ch0']:
    cfg_id[0] = get_channel_config_id(config['ch0'])
    p_ch0 = multiprocessing.Process(target=channel_data_packer, args=(child_conn_ch0, 0, parent_conn_sender))
    p_ch0.start()

if config['ch1']:
    cfg_id[1] = get_channel_config_id(config['ch1'])
    p_ch1 = multiprocessing.Process(target=channel_data_packer, args=(child_conn_ch1, 1, parent_conn_sender))
    p_ch1.start()

if config['ch2']:
    cfg_id[2] = get_channel_config_id(config['ch2'])
    p_ch2 = multiprocessing.Process(target=channel_data_packer, args=(child_conn_ch2, 2, parent_conn_sender))
    p_ch2.start()

if config['ch3']:
    cfg_id[3] = get_channel_config_id(config['ch3'])
    p_ch3 = multiprocessing.Process(target=channel_data_packer, args=(child_conn_ch3, 3, parent_conn_sender))
    p_ch3.start()

p_data_sender = multiprocessing.Process(target=data_sender, args=(child_conn_sender, ))
p_data_sender.start()

p_status_updater = multiprocessing.Process(target=status_updater, args=(child_conn_status, ))
p_status_updater.start()

p_main = multiprocessing.Process(target=spi_data_reader,
                                 args=(parent_conn_ch0,
                                       parent_conn_ch1,
                                       parent_conn_ch2,
                                       parent_conn_ch3,
                                       parent_conn_status))
p_main.start()
save_to_log("Egg is running...")

while True:
    if p_ch0.is_alive() is False:  # TODO: restart processes
        save_to_log("p_ch0 problem - restarting")
        p_ch0 = multiprocessing.Process(target=channel_data_packer, args=(child_conn_ch0, 0, parent_conn_sender))
        p_ch0.start()

    if p_data_sender.is_alive() is False:
        save_to_log("p_data_sender problem - restarting")
        p_data_sender = multiprocessing.Process(target=data_sender, args=(child_conn_sender,))
        p_data_sender.start()

    if p_status_updater.is_alive() is False:
        save_to_log("p_status_updater problem - restarting")
        p_status_updater = multiprocessing.Process(target=status_updater, args=(child_conn_status,))
        p_status_updater.start()

    if p_main.is_alive() is False:
        save_to_log("p_main problem - restarting")
        p_main = multiprocessing.Process(target=spi_data_reader,
                                         args=(parent_conn_ch0,
                                               parent_conn_ch1,
                                               parent_conn_ch2,
                                               parent_conn_ch3,
                                               parent_conn_status))
        p_main.start()

    time.sleep(1)

# p_main.join()
# TODO: is process alive?
