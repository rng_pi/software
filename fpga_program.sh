#!/bin/sh

openocd -f /dev/shm/software/raspberrypi4_openocd.cfg -f /usr/local/share/openocd/scripts/cpld/xilinx-xc7.cfg -c "init; xc7_program xc7.tap; pld load 0 $1; exit"

