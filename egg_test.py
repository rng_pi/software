#!/usr/bin/python3
import time

from egg_device import *

NUM_OF_BITS_TO_COUNT = 500 * 1000 * 1000

ch = 0

serial_number = get_serial_number()

egg = EggDevice()
egg.reset()

# egg.test_enable_ctrl(1)
egg.test_enable_ctrl(0)

egg.ch_enable_ctrl(channel=ch, state=OFF)
egg.set_num_of_bits_to_count(channel=ch, num=NUM_OF_BITS_TO_COUNT)
egg.set_xor_val(channel=ch, subchannel=2, val=1)
egg.set_xor_val(channel=ch, subchannel=3, val=2)
egg.set_xor_val(channel=ch, subchannel=4, val=4)
egg.set_xor_val(channel=ch, subchannel=5, val=8)
egg.set_xor_val(channel=ch, subchannel=6, val=16)
egg.set_xor_val(channel=ch, subchannel=7, val=32)
egg.ch_enable_ctrl(channel=0, state=ON)
egg.ch_enable_ctrl(channel=1, state=OFF)
egg.ch_enable_ctrl(channel=2, state=OFF)
egg.ch_enable_ctrl(channel=3, state=OFF)

ts_diff = 0
ts_diff_max = 0
ts1 = time.time()

while True:
    # status = egg.read_status()
    # print(status)
    # print(egg.read_gps_data())
    status, data = egg.read_channel_data(channel=ch)
    print(status)
    print(data)
    print(egg.parse_channel_data(data))
    # # ts_sec, ts_nanosec = egg.parse_timestamp_sec_nanosec(data[0:10])
    # # print(ch, datetime.now().time(), ts_sec, ts_nanosec)
    time.sleep(0.01)
    # print(time.time())
