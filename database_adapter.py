from requests.exceptions import ConnectionError, RequestException, HTTPError
import requests
import json
import datetime


class DatabaseAdapter:
    def __init__(self, server_address):
        self.server_address = server_address

    def upload_channel_config(self,
                              num_of_bits_to_count,
                              xor0_conf,
                              xor1_conf,
                              xor2_conf,
                              xor3_conf,
                              xor4_conf,
                              xor5_conf,
                              xor6_conf,
                              xor7_conf):
        url = 'http://{address}/upload_config/'.format(address=self.server_address)
        try:
            r = requests.post(url,
                              json={"num_of_bits_to_count": num_of_bits_to_count,
                                    "xor0_conf": xor0_conf,
                                    "xor1_conf": xor1_conf,
                                    "xor2_conf": xor2_conf,
                                    "xor3_conf": xor3_conf,
                                    "xor4_conf": xor4_conf,
                                    "xor5_conf": xor5_conf,
                                    "xor6_conf": xor6_conf,
                                    "xor7_conf": xor7_conf}
                              )

            if r.status_code == 404:
                print("Serial number not found")

            # raise HTTPError if 400 <= status code <= 600
            r.raise_for_status()

            # raise ValueError if json is not valid
            data = r.json()

            return data

        except (ConnectionError, HTTPError, ValueError) as err:
            raise err

    def update_device_status(self, serial_number: str, gps_signal_valid: bool, gps_position: str = None):
        url = 'http://{address}/status_update/{sn}/'.format(address=self.server_address, sn=serial_number)
        try:
            r = requests.post(url, json={"gps_signal_valid": gps_signal_valid,
                                         "gps_position": gps_position})

            if r.status_code == 404:
                print("Serial number not found")

            # raise HTTPError if 400 <= status code <= 600
            r.raise_for_status()

            # raise ValueError if json is not valid
            data = r.json()

            return data

        except (ConnectionError, HTTPError, ValueError) as err:
            raise err

    def upload_data_binary(self, serial_number: str, channel: int, config_id: int, data_file_path: str, crc32: int):
        url = 'http://{address}/upload_data_binary/{sn}/'.format(address=self.server_address, sn=serial_number)
        try:
            description = {'channel': channel,
                           'config_id': config_id,
                           'crc32': crc32}
            files = [
                ('binary_data', (data_file_path, open(data_file_path, 'rb'), 'application/octet')),
                ('description', ('description', json.dumps(description), 'application/json')),
            ]
            r = requests.post(url, files=files)
            if r.status_code == 404:
                print("Serial number not found")

            # raise HTTPError if 400 <= status code <= 600
            r.raise_for_status()

            # raise ValueError if json is not valid
            data = r.json()

            return data

        except (ConnectionError, HTTPError, ValueError) as err:
            raise err
